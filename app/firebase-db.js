import Firebase from 'firebase';

var firebaseApp = Firebase.initializeApp({
  apiKey: "AIzaSyCW55UvgZzauQ9XERUDkquoPboP28HIUbE",
  authDomain: "vuejs-firebase-a13d0.firebaseapp.com",
  databaseURL: "https://vuejs-firebase-a13d0.firebaseio.com",
  projectId: "vuejs-firebase-a13d0",
  storageBucket: "vuejs-firebase-a13d0.appspot.com",
  messagingSenderId: "665694260330"
});
export default firebaseApp.database();

